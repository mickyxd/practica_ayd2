# imports
import json
import sys
import time
import os
import random
import string
import time
#import datetime
#from selenium.webdriver.support.ui import WebDriverWait
#from selenium.webdriver.common.by import By
#from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.select import Select
from random import randrange
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.common.keys import Keys

SELENIUM_HUB = 'http://34.125.41.136:4445/wd/hub'
driver = webdriver

host = 'http://35.232.243.74/'
#host = 'http://localhost:4200/'

# Miguel Angel Solis Yantuche
# 201700543

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def test1():
    try:
        print("starting prueba crear")
        driver = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX,
        )

        print("opening the page")
        driver.get(host)
        time.sleep(4)
        print("se abrio la pagina")

        search_btn = driver.find_element_by_id("nuevo")
        search_btn.click()
        time.sleep(4)
        print("se abrio otra pagina")

        search_u = driver.find_element_by_id("name")
        search_u.clear()
        search_u.send_keys(id_generator())
        time.sleep(1)
        print("se escribio name")

        search_e = driver.find_element_by_id("edad")
        search_e.clear()
        search_e.send_keys(id_generator())
        time.sleep(1)
        print("se escribio edad")

        search_btn = driver.find_element_by_id("add_button")
        search_btn.click()
        time.sleep(4)
        print("se inserto el usuario")

        print("prueba de registro exitosa")
        time.sleep(10)
        driver.quit()
        return(1)
    except:
        driver.quit()
        print("error en prueba de registro")
        return (0)


def test2():
    try:
        print("starting prueba editar")
        driver = webdriver.Remote(
            command_executor=SELENIUM_HUB,
            desired_capabilities=DesiredCapabilities.FIREFOX
        )

        print("opening the page")
        driver.get(host)
        time.sleep(4)
        print("se abrio la pagina")

        print("abrir editor")
        search_btn = driver.find_element_by_id("btn_0")
        search_btn.click()
        time.sleep(4)
        print("se abrio editor")


        search_u = driver.find_element_by_id("name")
        search_u.clear()
        search_u.send_keys(id_generator())
        time.sleep(1)
        print("se escribio name")

        search_e = driver.find_element_by_id("edad")
        search_e.clear()
        search_e.send_keys(id_generator())
        time.sleep(1)
        print("se escribio edad")

        search_btn = driver.find_element_by_id("add_button")
        search_btn.click()
        time.sleep(4)
        print("se inserto el usuario")

        print("prueba de editar exitosa")
        time.sleep(10)
        driver.quit()
        return(1)
    except Exception as e:
        driver.quit()
        print("error en prueba de usuario no existente")
        return (0)


def __main__():
    msgError = "Error en las pruebas funcionales."

    if(test1() == 0):
        raise Exception(msgError)
    if(test2() == 0):
        raise Exception(msgError)

    print("Pruebas Funcionales Exitosas")


__main__()
