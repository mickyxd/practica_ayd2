import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

interface usr{
  id: string
  nombre: string;
  edad: string;
}

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})

export class ModalComponent implements OnInit {
  @Input() public usuario:usr = {id:'',nombre:'', edad:'-1'};

  id = '';
  name = '';
  edad = '';

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit(): void {
    console.log(this.usuario)
    if (this.usuario.edad != '-1') {
      this.id = this.usuario.id;
      this.name = this.usuario.nombre;
      this.edad = this.usuario.edad;
    }
  }

  addReport() {
    this.usuario = {id:this.id, nombre:this.name, edad:this.edad};

    this.activeModal.dismiss('')
  }

  modificar() {
    this.usuario = {id:this.id, nombre:this.name, edad:this.edad};

    this.activeModal.dismiss('')
  }
}
