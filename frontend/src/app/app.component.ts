import { Component } from '@angular/core';
import { ModalComponent } from './modal/modal.component'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpParams } from "@angular/common/http";


interface usr{
  id: string;
  nombre: string;
  edad: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  usuarios: usr[] = [];

  constructor(private modalService: NgbModal, private http: HttpClient) { }

  ngOnInit() {
    let us:usr = {id:'0', nombre:'hola',edad:'asdf'};
    this.usuarios.push(us)
  }

  add_report() {
    // Add a new Report
    const modalRef = this.modalService.open(ModalComponent);

    modalRef.result.then((result) => {
      this.ngOnInit();
    }).catch((error) => {
      console.log(error);
      let nuevo = modalRef.componentInstance.usuario
      if(nuevo.edad != '-1'){
        nuevo.id = this.usuarios.length+'';
        this.usuarios.push(nuevo);
      }
    });
  }

  editar(usuario:usr) {
    // Edit the usuario
    const modalRef = this.modalService.open(ModalComponent);
    modalRef.componentInstance.usuario = usuario;
    
    modalRef.result.then((result) => {
      this.ngOnInit();
    }).catch((error) => {
      console.log(error);
      let editado = modalRef.componentInstance.usuario
      if(editado.edad != '-1'){
        this.usuarios[editado.id] = editado;
      }
    });
    
  }
}
